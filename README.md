# Instruksi

## Buat GCP service account

Lihat project ID yang digunakan :

```
gcloud projects list
```

Set project ID yang akan digunakan :

```
export PROJECT_ID=<ISI-DENGAN-PROJECT-ID>
```

Buat sebuah service account yang akan digunakan oleh Gitlab Runner untuk mengambil credential Kubernetes Clusternya :

```
gcloud iam service-accounts create k8s-runner --display-name k8s-runner --description "k8s runner"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:k8s-runner@$PROJECT_ID.iam.gserviceaccount.com" --role="roles/logging.logWriter"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:k8s-runner@$PROJECT_ID.iam.gserviceaccount.com" --role="roles/monitoring.metricWriter"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:k8s-runner@$PROJECT_ID.iam.gserviceaccount.com" --role="roles/container.developer"
```

Buat sebuah key yang akan dipasang di Gitlab sebagai sebuah variable :

```
gcloud iam service-accounts keys create ./k8s-runner.json --iam-account=k8s-runner@$PROJECT_ID.iam.gserviceaccount.com
```

Lakukan encode sederhana :

```
base64 k8s-runner.json
```

## Membuat variable di Gitlab

### Variable untuk GCP

Buat sebuah variable di project Gitlab. Copy output dari command di atas dan paste di bagian `Value`. Beri nama variablenya `SERVICE_ACCOUNT_KEY` :

![](images/2023-02-24_22-38.png)
![](images/2023-02-24_22-42.png)

Buat sebuah variable dengan nama `GKE_CLUSTER_NAME` dan `GKE_CLUSTER_ZONE` yang berisi nama dari Kubernetes clusternya dan Zone-nya.

### Variable untuk Dockerhub

Buat variable untuk login ke Dockerhub dengan nama `CI_REGISTRY_PASSWORD` dan `CI_REGISTRY_USERNAME`. Isi dengan username dan password dari Dockerhub.

## Membuat Namespace dan Ingress

Masing-masing environment (production dan staging) akan dipisah menjadi 2 "folder" atau disebut dengan Namespace. Ingress juga dibedakan agar tidak saling mengganggu satu sama lain.

Buat 2 namespace yang akan digunakan untuk memisahkan project Staging dan Production :

```
kubectl get ns
kubectl create ns staging
kubectl create ns production
kubectl get ns
```

Buat masing-masing ingress untuk production dan development :

```
kubectl apply -f deployment/production/ingress.yaml
kubectl apply -f deployment/staging/ingress.yaml
```